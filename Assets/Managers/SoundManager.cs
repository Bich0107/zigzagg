﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoSingleton<SoundManager>
{
    [SerializeField] AudioSource loopSource;
    [SerializeField] AudioSource sfxSource;

    [Header("BGMs")]
    [SerializeField] AudioClip bgm1;

    [Header("Sounds")]
    [SerializeField] AudioClip collideItemSound;
    [SerializeField] AudioClip clickSound;

    [Header("Volumes")]
    public float baseMusicVolume;
    public float baseSoundVolume;
    public float normalMusicVolume;
    public float normalSoundVolume;

    protected override void Awake()
    {
        base.Awake();
    }

    public void PlayCollideItemSound()
    {
        sfxSource.clip = collideItemSound;
        sfxSource.Play();
    }

    public void PlayClickSound()
    {
        sfxSource.clip = clickSound;
        sfxSource.Play();
    }

    public void MusicVolumeChange(float volume)
    {
        loopSource.volume = volume;
    }

    public void SoundVolumeChange(float volume)
    {
        sfxSource.volume = volume;
    }
}

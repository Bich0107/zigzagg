﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoSingleton<SpawnManager>
{
    [SerializeField] float itemHeight;

    [Range(0, 100)]
    [SerializeField] float spawnItemChance;

    public GameObject GenerateRandomItem(GameObject item, Vector3 pos)
    {
        int randomNumber = Random.Range(1, 101);

        if (randomNumber > (100 - spawnItemChance))
        {
            pos.y += itemHeight;
            item.transform.position = pos;
            item.SetActive(true);
            return item;
        }

        return null;
    }
}

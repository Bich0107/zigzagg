﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoSingleton<UIManager>
{
    public const string BUTTON_TAG = "Button";

    [Header("Panels")]
    [SerializeField] GameObject startPanel;
    [SerializeField] GameObject playPanel;
    [SerializeField] GameObject gameOverPanel;
    [SerializeField] GameObject pausePanel;
    [SerializeField] GameObject settingPanel;

    [Header("Score fields")]
    [SerializeField] Text bestScoreText;
    public Text scoreText_Play;
    public Text scoreText_End;

    [Header("Congrats text")]
    [SerializeField] GameObject congratsText;

    [Header("Actions")]
    public Action e_HideStartPanel;
    public Action e_HidePlayPanel;
    public Action e_HideGameOverPanel;
    public Action e_HidePausePanel;
    public Action e_HideSettingPanel;

    [Header("Flags")]
    [SerializeField] bool isStartPanelShowed;
    [SerializeField] bool isPlayPanelShowed;
    [SerializeField] bool isGameOverPanelShowed;
    [SerializeField] bool isPausePanelShowed;
    [SerializeField] bool isSettingPanelShowed;

    protected override void Awake()
    {
        base.Awake();
        isStartPanelShowed = true;
        isPlayPanelShowed = false;
        isGameOverPanelShowed = false;
        isPausePanelShowed = false;
        isSettingPanelShowed = false;
    }

    public void PlayButtonClicked()
    {
        GameManager.Instance.StartGame();
    }

    public void ToggleStartPanel()
    {
        TogglePanel(startPanel, ref isStartPanelShowed, e_HideStartPanel);
    }

    public void TogglePausePanel()
    {
        TogglePanel(pausePanel, ref isPausePanelShowed, e_HidePausePanel);
    }

    public void ToggleSettingPanel()
    {
        if (!startPanel.activeSelf) TogglePausePanel();

        TogglePanel(settingPanel, ref isSettingPanelShowed, e_HideSettingPanel);
    }

    public void TogglePlayPanel()
    {
        TogglePanel(playPanel, ref isPlayPanelShowed, e_HidePlayPanel);

        DisplayScore(scoreText_Play);
    }

    public void ToggleGameoverPanel()
    {
        if (DataManager.Instance.UpdateScore())
        {
            congratsText.SetActive(true);
            DisplayHighestScore(scoreText_End);
        }
        else
        {
            DisplayScore(scoreText_End);
        }

        TogglePanel(gameOverPanel, ref isGameOverPanelShowed, e_HideGameOverPanel);
    }

    public void TogglePanel(GameObject panel, ref bool flag, Action action)
    {
        if (flag) action?.Invoke();
        else panel.SetActive(!panel.activeSelf);

        flag = !flag;
    }

    public void DisplayHighestScore(Text textField)
    {
        textField.text = PlayerPrefs.GetInt(DataManager.HIGHEST_SCORE_KEY).ToString();
    }

    public void DisplayScore(Text textField)
    {
        textField.text = PlayerPrefs.GetInt(DataManager.SCORE_KEY).ToString();
    }

    public void Reset()
    {
        ToggleStartPanel();

        DisplayHighestScore(bestScoreText);
        congratsText.SetActive(false);
    }
}

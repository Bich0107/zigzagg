﻿using UnityEngine;
using DG.Tweening;

public class GameManager : MonoSingleton<GameManager>
{
    [Header("Game Objects")]
    public GameObject player;

    [Header("Components")]
    [SerializeField] Camera mainCam;
    public Player playerScript;

    [Header("Flags")]
    public bool isChangingTheme;
    public bool isPause;
    public bool isGameOver;

    protected override void Awake()
    {
        base.Awake();
        isGameOver = false;
        isPause = false;
    }

    private void Start()
    {
        playerScript = player.GetComponent<Player>();
        PostProcessManager.Instance.SetRandomThemeImmediately();
    }

    public void StartGame()
    {
        playerScript.Run();

        SoundManager.Instance.PlayClickSound();

        UIManager.Instance.ToggleStartPanel();
        UIManager.Instance.TogglePlayPanel();

        isChangingTheme = true;
    }

    public void GameOver()
    {
        playerScript.Stop();
        isGameOver = true;

        UIManager.Instance.TogglePlayPanel();
        UIManager.Instance.ToggleGameoverPanel();
        
        MapGenerator.Instance.StopGenerateMap();
        DataManager.Instance.UpdateScore();

        isChangingTheme = true;
    }

    public void Pause()
    {
        isPause = true;
        SoundManager.Instance.PlayClickSound();
        UIManager.Instance.TogglePlayPanel();
        UIManager.Instance.TogglePausePanel();
        Time.timeScale = 0;
    }

    public void Resume()
    {
        isPause = false;
        SoundManager.Instance.PlayClickSound();
        UIManager.Instance.TogglePausePanel();
        UIManager.Instance.TogglePlayPanel();
        Time.timeScale = 1;
    }

    public void ResetAll()
    {
        playerScript.Reset();
        isGameOver = false;

        ObjectPool.Instance.Reset();
        UIManager.Instance.Reset();
        DataManager.Instance.Reset();
        MapGenerator.Instance.Reset();
        PostProcessManager.Instance.SetRandomThemeImmediately();
        PostProcessManager.Instance.Reset();

        DOTween.RestartAll();

        isChangingTheme = false;
    }

    public void ReturnToStartScreen()
    {
        isPause = false;
        SoundManager.Instance.PlayClickSound();
        ResetAll();
        Time.timeScale = 1;
        UIManager.Instance.TogglePausePanel();
    }

    public void Retry()
    {
        SoundManager.Instance.PlayClickSound();
        UIManager.Instance.ToggleGameoverPanel();
    }
}

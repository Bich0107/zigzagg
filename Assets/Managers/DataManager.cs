﻿using UnityEngine;

public class DataManager : MonoSingleton<DataManager>
{
    public const string SCORE_KEY = "SCORE";
    public const string HIGHEST_SCORE_KEY = "HIGHEST_SCORE";

    [Header("Game objects")]
    [SerializeField] GameObject player;

    [Header("Score calculator")]
    [SerializeField] int scoreBase;
    [SerializeField] float scoreMultiplier;
    public float BonusScoreTime;

    private int _score;
    public int Score
    {
        get
        {
            return _score;
        }
        set
        {
            _score = value;
            PlayerPrefs.SetInt(SCORE_KEY, _score);
            UIManager.Instance.DisplayScore(UIManager.Instance.scoreText_Play);
        }
    }

    private int _bestScore;
    public int BestScore
    {
        get
        {
            return _bestScore;
        }
        set
        {
            _bestScore = value;
            PlayerPrefs.SetInt(HIGHEST_SCORE_KEY, _bestScore);
            Score = 0;
        }
    }

    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        player = GameManager.Instance.player;

        BestScore = 0;
        Score = 0;
    }

    private void FixedUpdate()
    {
        if (player.GetComponent<Player>().isRunning)
        {
            ScoreCounting();
        }
    }

    public void ScoreCounting()
    {
        if (BonusScoreTime > 0)
        {
            BonusScoreTime -= Time.deltaTime;
            Score += Mathf.RoundToInt(scoreBase * scoreMultiplier * 2);
        }
        else Score += Mathf.RoundToInt(scoreBase * scoreMultiplier);
    }

    public bool UpdateScore()
    {
        if (Score > BestScore)
        {
            BestScore = Score;
            return true;
        }
        else return false;
    }

    public void Reset()
    {
        Score = 0;
        BonusScoreTime = 0;
    }
}

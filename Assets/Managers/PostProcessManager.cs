﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessManager : MonoSingleton<PostProcessManager>
{
    [Header("Components")]
    [SerializeField] PostProcessVolume postProcessVolume;

    [Header("Hue shift values list")]
    [Range(-180, 180)]
    [SerializeField] List<float> hueShiftValues;

    [Header("Other variables")]
    [SerializeField] int currentTheme;
    public float changeThemeInterval;
    public float timer;
    public float velocity;
    public float smoothTime;
    public int themeIndex;

    protected override void Awake()
    {
        base.Awake();
        currentTheme = 0;
        themeIndex = -1;
        timer = changeThemeInterval;
    }

    public void Update()
    {
        if (GameManager.Instance.isChangingTheme)
        {
            if (timer > 0)
            {
                if (themeIndex == -1) themeIndex = GetRandomThemeIndex();

                if (postProcessVolume.profile.TryGetSettings(out ColorGrading colorGrading))
                {
                    colorGrading.hueShift.value = Mathf.SmoothDamp(colorGrading.hueShift.value, hueShiftValues[themeIndex], ref velocity, smoothTime);
                }

                timer -= Time.deltaTime;
            }
            else
            {
                themeIndex = GetRandomThemeIndex();
                timer = changeThemeInterval;
                GameManager.Instance.playerScript.IncreaseSpeed();
            }
        }
    }

    public int GetRandomThemeIndex()
    {
        int random = Random.Range(0, hueShiftValues.Count);
        while (currentTheme == random)
        {
            // make sure next theme is different from the last one
            random = Random.Range(0, hueShiftValues.Count);
        }
        return random;
    }

    public void SetRandomThemeImmediately()
    {
        int random = GetRandomThemeIndex();

        if (postProcessVolume.profile.TryGetSettings(out ColorGrading colorGrading))
        {
            colorGrading.hueShift.value = hueShiftValues[random];
        }

        currentTheme = random;
    }

    public void Reset()
    {
        timer = changeThemeInterval;
        themeIndex = -1;
        velocity = 0;
    }
}

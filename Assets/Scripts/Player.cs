﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] MovingObject movingObject;
    [SerializeField] Animator animator;

    [Header("Speed control")]
    [Range(2f, 5f)]
    [SerializeField] float baseSpeed;
    [Range(2f, 5f)]
    [SerializeField] float speed;
    [SerializeField] float maxSpeed;
    [Range(0, 30f)]
    [SerializeField] float speedIncreasePercent;

    [Header("Other variables")]
    [SerializeField] int direction;  // -1 : left, 1 : right
    [SerializeField] Vector3 basePos;

    [Header("Flags")]
    public bool isRunning;

    private void Start()
    {
        AssignMovingObjectProperties();
    }

    private void OnValidate()
    {
        speed = baseSpeed;
    }

    private void Update()
    {
        if (transform.position.y <= 1f && !GameManager.Instance.isGameOver)
        {
            GameManager.Instance.GameOver();
        }
        else if (transform.position.y <= -30f) gameObject.SetActive(false);

        if (GameManager.Instance.isGameOver || GameManager.Instance.isPause) return;

        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.currentSelectedGameObject != null)
                if (EventSystem.current.currentSelectedGameObject.tag.Equals(UIManager.BUTTON_TAG))
                    return;

            transform.Rotate(0, direction * 90, 0, Space.Self);
            direction = -direction;
        }
#if UNITY_EDITOR
        else if (Input.GetMouseButtonDown(1)) // debug on pc
        {
            isRunning = !isRunning;
        }
#endif
    }

    private void FixedUpdate()
    {
        AssignMovingObjectProperties();
        if (GameManager.Instance.isGameOver) speed -= Time.deltaTime * speed;
    }

    private void AssignMovingObjectProperties()
    {
        movingObject.SetState(direction, speed, isRunning);
    }

    public void Run()
    {
        MapGenerator.Instance.StartDestroyMap();
        isRunning = true;
    }

    public void Stop() => isRunning = false;

    public void IncreaseSpeed()
    {
        speed = Mathf.Min(speed + speed * speedIncreasePercent / 100f, maxSpeed);
    }

    public void Reset()
    {
        transform.position = basePos;
        transform.rotation = Quaternion.identity;
        direction = 1;
        speed = baseSpeed;
        isRunning = false;
        gameObject.SetActive(true);
    }
}

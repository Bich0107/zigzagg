﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicVolumeController : MonoBehaviour
{
    [SerializeField] Slider volumeSlider;

    private void Start()
    {
        volumeSlider.value = SoundManager.Instance.baseMusicVolume;
    }

    public void VolumeChange()
    {
        SoundManager.Instance.MusicVolumeChange(volumeSlider.value);
    }
}

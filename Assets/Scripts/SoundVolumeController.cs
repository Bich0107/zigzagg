﻿using UnityEngine;
using UnityEngine.UI;

public class SoundVolumeController : MonoBehaviour
{
    [SerializeField] Slider volumeSlider;

    private void Start()
    {
        volumeSlider.value = SoundManager.Instance.baseSoundVolume;
    }

    public void VolumeChange()
    {
        SoundManager.Instance.SoundVolumeChange(volumeSlider.value);
    }
}

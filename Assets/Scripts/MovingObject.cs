﻿using UnityEngine;

public class MovingObject : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] Rigidbody rigid;

    [Header("Other variables")]
    [SerializeField] int direction;
    [SerializeField] float speed;
    [SerializeField] bool isRunning;

    private void FixedUpdate()
    {
        if (isRunning || GameManager.Instance.isGameOver)
        {
            Vector3 nextPos = transform.position;
            if (direction == 1)
            {
                nextPos.z += speed * Time.deltaTime;
            }
            else
            {
                nextPos.x += speed * Time.deltaTime;
            }
            rigid.MovePosition(nextPos);
        }
    }

    public void SetState(int dir, float spd, bool flag)
    {
        direction = dir;
        speed = spd;
        isRunning = flag;
    }
}

﻿using UnityEngine;
using DG.Tweening;
using static UnityEngine.ParticleSystem;

public class ScoreUpItemScript : MonoBehaviour
{
    [Header("Game Objects")]
    [SerializeField] GameObject player;

    [Header("Components")]
    [SerializeField] ParticleSystem burstParticle;
    [SerializeField] MeshRenderer meshRenderer;

    [Header("Flags")]
    public bool collided;
    public bool isSelfDestroying;

    [Header("Animation variables")]
    [SerializeField] Vector3 startingPos;
    [SerializeField] float jumpRange;
    Sequence sequence;

    [Header("Other variables")]
    public float bonusScoreTime;

    private void Start()
    {
        player = GameManager.Instance.player;
        startingPos = player.transform.position;
        burstParticle = transform.GetChild(0).GetComponent<ParticleSystem>();
        isSelfDestroying = false;
    }

    private void OnEnable()
    {
        meshRenderer.material.DOFade(0f, 1f);
        isSelfDestroying = false;
    }

    private void OnDisable()
    {
        var emission = burstParticle.emission;
        emission.enabled = false;
        collided = false;
        sequence.Kill(true);
    }

    private void Update()
    {
        if (IsBehindPlayer() && !isSelfDestroying)
        {
            isSelfDestroying = true;
            transform.DOKill(true);
            transform.DOMoveY(10, 2f).SetUpdate(true).OnComplete(DestroyMyself);
        }
    }

    private bool IsBehindPlayer()
    {
        return (transform.position - startingPos).magnitude < (player.transform.position - startingPos).magnitude - 1f && !collided && !GameManager.Instance.isGameOver;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Collide();

        DataManager.Instance.BonusScoreTime = bonusScoreTime;
        SoundManager.Instance.PlayCollideItemSound();

        // create a random pos to jump to
        var jumpPos = transform.position;
        jumpPos.x = Random.Range(jumpPos.x - jumpRange, jumpPos.x + jumpRange);
        jumpPos.y += 0.5f;
        jumpPos.z = Random.Range(jumpPos.z - jumpRange, jumpPos.z + jumpRange);

        sequence = DOTween.Sequence();
        sequence.Append(transform.DOJump(jumpPos, 0.5f, 1, 0.75f));
        sequence.Append(transform.DOMoveY(10f, 2f)).SetUpdate(true).OnComplete(DestroyMyself);
    }

    private void Collide()
    {
        collided = true;
        var emission = burstParticle.emission;
        emission.enabled = true;
        burstParticle.Play();
    }

    public void DestroyMyself()
    {
        transform.DOKill(true);
        gameObject.SetActive(false);
    }
}

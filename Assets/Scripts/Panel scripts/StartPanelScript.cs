﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StartPanelScript : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] MoveAnimatedGO titleScript;
    [SerializeField] MoveAnimatedGO playButtonScript;
    [SerializeField] MoveAnimatedGO scoreFieldScript;
    [SerializeField] MoveAnimatedGO settingButtonScript;

    private void Start()
    {
        UIManager.Instance.e_HideStartPanel += titleScript.Disappear;
        UIManager.Instance.e_HideStartPanel += playButtonScript.Disappear;
        UIManager.Instance.e_HideStartPanel += scoreFieldScript.Disappear;
        UIManager.Instance.e_HideStartPanel += settingButtonScript.Disappear;
        UIManager.Instance.e_HideStartPanel += Disappear;
    }

    private void Disappear()
    {
        // a tween do nothing to wait for 1 second before self-destruct
        transform.DOMove(transform.position, 1f).SetUpdate(true).OnComplete(DestroyMyself);
    }

    private void DestroyMyself()
    {
        gameObject.SetActive(false);
    }
}

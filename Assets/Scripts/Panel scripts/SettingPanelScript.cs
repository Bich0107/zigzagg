﻿using UnityEngine;
using DG.Tweening;

public class SettingPanelScript : MonoBehaviour
{
    [Header("Script")]
    [SerializeField] MoveAnimatedGO panelScript;

    private void Start()
    {
        UIManager.Instance.e_HideSettingPanel += panelScript.Disappear;
        UIManager.Instance.e_HideSettingPanel += Disappear;
    }

    private void Disappear()
    {
        // a tween do nothing to wait for 1 second before self-destruct
        transform.DOPunchScale(Vector3.zero, 1, 0, 0).SetUpdate(true).OnComplete(DestroyMyself);
    }

    private void DestroyMyself()
    {
        gameObject.SetActive(false);
    }
}

﻿using UnityEngine;
using DG.Tweening;

public class GameOverPanelScript : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] MoveAnimatedGO titleScript;
    [SerializeField] MoveAnimatedGO retryButtonScript;
    [SerializeField] MoveAnimatedGO scoreFieldScript;
    [SerializeField] MoveAnimatedGO congrasTextScript;

    private void Start()
    {
        UIManager.Instance.e_HideGameOverPanel += titleScript.Disappear;
        UIManager.Instance.e_HideGameOverPanel += retryButtonScript.Disappear;
        UIManager.Instance.e_HideGameOverPanel += scoreFieldScript.Disappear;
        UIManager.Instance.e_HideGameOverPanel += congrasTextScript.Disappear;
        UIManager.Instance.e_HideGameOverPanel += Disappear;
        UIManager.Instance.e_HideGameOverPanel += GameManager.Instance.ResetAll;
    }

    private void Disappear()
    {
        // a tween do nothing to wait for 1 second before self-destruct
        transform.DOMove(transform.position, 1f).SetUpdate(true).OnComplete(DestroyMyself);
    }

    private void DestroyMyself()
    {
        gameObject.SetActive(false);
    }
}

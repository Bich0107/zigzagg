﻿using UnityEngine;
using DG.Tweening;

public class PlayPanelScript : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] MoveAnimatedGO scoreFieldScript;
    [SerializeField] MoveAnimatedGO pauseButtonScript;

    private void Start()
    {
        UIManager.Instance.e_HidePlayPanel += scoreFieldScript.Disappear;
        UIManager.Instance.e_HidePlayPanel += pauseButtonScript.Disappear;
        UIManager.Instance.e_HidePlayPanel += Disappear;
    }

    private void Disappear()
    {
        // a tween do nothing to wait for 1 second before self-destruct
        transform.DOMove(transform.position, 1f).SetUpdate(true).OnComplete(DestroyMyself);
    }

    private void DestroyMyself()
    {
        gameObject.SetActive(false);
    }
}

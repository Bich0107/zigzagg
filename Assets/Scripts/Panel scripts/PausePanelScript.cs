﻿using UnityEngine;
using DG.Tweening;

public class PausePanelScript : MonoBehaviour
{
    [Header("Script")]
    [SerializeField] MoveAnimatedGO panelScript;

    private void Start()
    {
        UIManager.Instance.e_HidePausePanel += panelScript.Disappear;
        UIManager.Instance.e_HidePausePanel += Disappear;
    }

    private void Disappear()
    {
        // a tween do nothing to wait for 1 second before self-destruct
        transform.DOPunchScale(Vector3.zero, 1f, 0, 0).SetUpdate(true).OnComplete(DestroyMyself);
    }

    private void DestroyMyself()
    {
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CubeScript : MonoBehaviour
{
    public const string DISAPPEAR_ANIMATION = "CubeDisappearing";

    [Header("Components")]
    [SerializeField] Animator animator;
    [SerializeField] MeshRenderer meshRenderer;

    private void OnCollisionExit(Collision collision)
    {
        if (MapGenerator.Instance.isDestroying)
        {
            PlayDisappearingAnimation();
        }
    }

    private void a_DestroyMyself()
    {
        gameObject.SetActive(false);
    }

    public void PlayDisappearingAnimation() => animator.Play(DISAPPEAR_ANIMATION);
}

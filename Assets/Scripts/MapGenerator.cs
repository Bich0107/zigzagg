﻿using System;
using System.Collections;
using UnityEngine;

public class MapGenerator : MonoSingleton<MapGenerator>
{
    // x + 1: generate to the left
    // z + 1: generate to the right
    [Header("Components")]
    [SerializeField] GameObject startingCube; // cube at the starting point of the game
    [SerializeField] Player player;

    // flag for cube to start self-destruct
    [Header("Flags")]
    public bool isDestroying;
    public bool isRandomDirection;
    public bool isGenerating;

    [Header("Timer variables")]
    [Tooltip("Timer for destroying stating square")]
    public float timer;
    [Range(0.1f, 0.5f)]
    [SerializeField] float generateInterval;

    [Header("Other variables")]
    [SerializeField] Vector3 currentPos;
    [SerializeField] int direction; // 0 : left || 1 : right
    [SerializeField] int squareEdgeLength;
    [SerializeField] Action e_DestroyStartingSquare;

    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        StartGenerateMap();
    }

    public void StartGenerateMap()
    {
        isGenerating = true;

        // get position to start creating
        currentPos = startingCube.transform.position;

        // generate first square
        currentPos = GenerateSquare(currentPos);

        StartCoroutine(CR_GeneratePath());
    }

    public void StartDestroyMap()
    {
        StartCoroutine(CR_StartDestroy());
    }

    public void StopGenerateMap() => isGenerating = false;

    public Vector3 GenerateSquare(Vector3 pos)
    {
        Vector3 startingPos = pos;

        // generate starting line cubes
        for (int i = 0; i < squareEdgeLength; i++)
        {
            GameObject newCube = ObjectPool.Instance.Spawn(ObjectPool.CUBE_TAG);
            if (newCube != null)
            {
                newCube.transform.position = startingPos;
                newCube.SetActive(true);
                e_DestroyStartingSquare += newCube.GetComponent<CubeScript>().PlayDisappearingAnimation;
                currentPos = newCube.transform.position;

                // generate cubes in each line
                for (int j = 0; j < squareEdgeLength - 1; j++)
                {
                    newCube = ObjectPool.Instance.Spawn(ObjectPool.CUBE_TAG);
                    if (newCube != null)
                    {
                        newCube.transform.position = new Vector3(
                        currentPos.x + j + 1,
                        currentPos.y,
                        currentPos.z
                        );
                        newCube.SetActive(true);
                        e_DestroyStartingSquare += newCube.GetComponent<CubeScript>().PlayDisappearingAnimation;
                    }
                }

                if (i == squareEdgeLength - 1)
                {
                    // return pos of last generated cube
                    return new Vector3(startingPos.x + squareEdgeLength - 1, startingPos.y, startingPos.z);
                }
                else
                {
                    startingPos.z += 1;
                }
            }
        }
        return startingPos;
    }

    public void DestroyStartingSquare()
    {
        e_DestroyStartingSquare?.Invoke();
    }

    public void Reset()
    {
        isDestroying = false;
        currentPos = startingCube.transform.position;
        e_DestroyStartingSquare = null;
        StartGenerateMap();
    }

    private void SetupRandomCube(GameObject gObject)
    {
        var newCube = gObject;

        if (isRandomDirection) direction = UnityEngine.Random.Range(0, 2);

        if (direction == 1)
        {
            currentPos.z += 1; // cube to the right
        }
        else
        {
            currentPos.x += 1; // cube to the left
        }
        newCube.transform.position = currentPos;
        newCube.SetActive(true);
    }

    IEnumerator CR_StartDestroy()
    {
        yield return new WaitForSecondsRealtime(timer);
        DestroyStartingSquare();
        isDestroying = true;
    }

    IEnumerator CR_GeneratePath()
    {
        if (isGenerating)
        {
            yield return new WaitForSecondsRealtime(generateInterval);

            var newCube = ObjectPool.Instance.Spawn(ObjectPool.CUBE_TAG);
            var newItem = ObjectPool.Instance.Spawn(ObjectPool.ITEM_TAG);
            if (newCube != null && newItem != null)
            {
                SetupRandomCube(newCube);
                SpawnManager.Instance.GenerateRandomItem(newItem, newCube.gameObject.transform.position);
            }

            yield return CR_GeneratePath();
        }
    }
}

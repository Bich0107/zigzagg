﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] GameObject targetObject;

    [SerializeField] Vector3 basePos;
    [SerializeField] Vector3 _cameraOffset;

    [Range(0.01f, 1f)]
    public float smoothFactor = 0.5f;

    private void Start()
    {
        _cameraOffset = transform.position - targetObject.transform.position;
    }

    private void LateUpdate()
    {
        if (!GameManager.Instance.isGameOver)
        {
            Vector3 newPos = targetObject.transform.position + _cameraOffset;
            transform.position = Vector3.Slerp(transform.position, newPos, smoothFactor);
        }
    }
}
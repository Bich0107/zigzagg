﻿using UnityEngine;
using DG.Tweening;

public class MoveAnimatedGO : MonoBehaviour
{
    [SerializeField] Vector3 startPos;
    [SerializeField] Vector3 endPos;
    [SerializeField] float duration;

    private void OnEnable()
    {
        transform.localPosition = startPos;
        transform.DOLocalMove(endPos, duration).SetUpdate(true).SetAutoKill(false);
    }

    public void Disappear()
    {
        transform.DOLocalMove(startPos, duration).SetUpdate(true).SetAutoKill(false);
    }
}

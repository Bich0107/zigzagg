﻿using UnityEngine;
using DG.Tweening;

public class PScaleAnimatedGO : MonoBehaviour
{
    [Header("Animation control")]
    [SerializeField] float punchScaleRate;
    [SerializeField] float duration;
    [SerializeField] LoopType loopType;

    private void OnEnable()
    {
        transform.localScale = Vector3.one;
        transform.DOScale(Vector3.one * punchScaleRate, duration).SetLoops(-1, loopType).SetUpdate(true);
    }
}
